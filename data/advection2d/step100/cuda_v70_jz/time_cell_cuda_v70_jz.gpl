set output "time_cell_cuda_v70_jz.tex"
set term epslatex standalone color ',,12'

set xlabel "dof/nGPU"
set ylabel '(computing time)$\times$nGPU/dof'
set logs
set format y "$10^{%L}$"
set format x "$10^{%L}$"	
#set xra [1e5:2e9]
set xra [1e3:]
set yra [:1e-3]
#set key left
set grid x
set key spacing 0.8
set key at screen 0.35, 0.48
set multiplot

factor=7./4 # compensation for RK4 to RK6
plot "chrono-ndg-6-4-cuda-v70-1-1.txt" u ($1**2*36):(($2/$1**2/36*factor)) t "1" dt 1  lw 4 w l, "chrono-ndg-6-4-cuda-v70-1-2.txt" u ($1**2*36/2):(($2/$1**2/36*factor*2)) t "2" dt 1  lw 4 w l,"chrono-ndg-6-4-cuda-v70-1-4.txt" u ($1**2*36/4):(($2/$1**2/36*factor*4)) t "4" dt 1 lw 4 w l,"chrono-ndg-6-4-cuda-v70-1-8.txt" u ($1**2*36/8):(($2/$1**2/36*factor*8)) t "8" lc 4 dt 1  lw 4 w l,"chrono-ndg-6-4-cuda-v70-1-16.txt" u ($1**2*36/16):(($2/$1**2/36*factor*16)) t "16" lc 6 dt 1  lw 4 w l,"chrono-ndg-6-4-cuda-v70-1-32.txt" u ($1**2*36/32):(($2/$1**2/36*factor*32)) t "32" lc 7 dt 1  lw 4 w l

#plot "chrono-ndg-6-4-cuda-v70-1-1.txt" u ($1**2*36):(($2/$1**2/36*factor)) t "1 GPU\\phantom{s}" dt 1  lw 4 w l, "chrono-ndg-6-4-cuda-v70-1-2.txt" u ($1**2*36/2):(($2/$1**2/36*factor*2)) t "2 GPUs" dt 1  lw 4 w l,"chrono-ndg-6-4-cuda-v70-1-4.txt" u ($1**2*36/4):(($2/$1**2/36*factor*4)) t "4 GPUs" dt 1 lw 4 w l,"chrono-ndg-6-4-cuda-v70-1-8.txt" u ($1**2*36/8):(($2/$1**2/36*factor*8)) t "8 GPUs" lc 4 dt 1  lw 4 w l,"chrono-ndg-6-4-cuda-v70-1-16.txt" u ($1**2*36/16):(($2/$1**2/36*factor*16)) t "16 GPUs" lc 6 dt 1  lw 4 w l,"chrono-ndg-6-4-cuda-v70-1-32.txt" u ($1**2*36/32):(($2/$1**2/36*factor*32)) t "32 GPUs" lc 7 dt 1  lw 4 w l

#,"chrono-ndg-6-6-cuda-v70-1-128.txt" u ($1**2*36/64):(($2/$1**2/36*64)) t "64" lc 7 dt 1  lw 4 w l,"chrono-ndg-6-6-cuda-v70-1-256.txt" u ($1**2*36/128):(($2/$1**2/36*128)) t "128" lc 8 dt 1  lw 4 w l

set origin 0.42,0.4
set size 0.55,0.55

set auto
set xlabel "nGPU"
set ylabel 'computing time'
set yra [0.4:20]
set xra [0.7:64]
unset grid

plot "strong_scaling.txt" t "" lw 4 w lp, [1:16] x**-1*15 t "" lw 4#t "$nGPU^{-1}$" lw 4